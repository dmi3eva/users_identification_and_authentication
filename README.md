Data files:
1. Unprocessed folder consists of sensor’s data from 6 users (only walking activity). In each folder there are 15 files with corresponding measures.
2.  Extracted_features folder contains files with normalized values after feature selection. “N.csv” - processed data from user number N.
3.  Selected_features – user’s data after PCA.
4. Features_fusion – values after feature fusion. Besides that contains separate time and frequency features in corresponding folders.
5. Temp – directory with temporary files for classification tasks.


To run the code are needed:
For Windows
Anaconda (https://docs.continuum.io/anaconda/install/)
Required Packages:
Scikit-learn (http://scikit-learn.org/stable/install.html). 
Command line: pip install  scikit-learn
Sklearn-extensions (http://wdm0006.github.io/sklearn-extensions/index.html)
Command line: pip install sklearn-extensions
For feature fusion and GAN tasks:
Tensorflow (https://www.tensorflow.org/install/install_windows#installing_with_anaconda)
Keras (https://keras.io/#installation)


Project files:
1. features_extraction_and_normalization.ipynb processes files from folder directory_from= “unprocessed” and put normalized values at folder directory_to=“extracted_features”. To change input and destination folders it is necessary to change corresponding variables. In directory “unprocessed/users/” should be folders with names “userN”, where N – the number of user.
2. feature_selection.ipynb reduces data dimension using PCA. It takes values from directory_from=“extracted_features” and puts results in directory_to=“features_fusion”.
3. feature_fusion.ipynb takes time features values from  “features_fusion/time” folder and frequency features values from “features_fusion/frequency” folder. It returns results in the root of  “features_fusion” folder.
4.  authentication_all_classifiers.ipynb defines users using different classifiers.
It takes files from directory_from= “extracted features”.
Function make_label(directory, 1user_num, not_user_nums) prepares data for training and testing marking user_num’s values with 1 and data from list of user not_user_nums with 0.
Data for train and testing saves in temporary files, so function make_label can be commented during testings with the same input data.
Function spoil_data(data, percentage) marks percentage% of data with wrong labels.
For all classifiers sorts out different parameters and choses ones with best scores.
5.  authentication_all_classifiers_pca.ipynb – the same procedure, but takes data from directory “selected_features”.
6. authentication_all_classifiers_fusion.ipynb – classifies data from directory “features_fusion”.
7.identification_all_classifiers.ipynb,  identification_all_classifiers_pca.ipynb, identification_all_classifiers_fusion.ipynb identifies users with different classifiers. Uses the same principle as authentication, but function  make_label(directory, 1user_num, not_user_nums) marks data with numbers of corresponding users (not 0 and 1).



